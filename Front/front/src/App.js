import React, {Component} from 'react';
import './App.css';

import{
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

import Novousuario from './components/Novousuario'
import getUser from './components/getUser'
import allUsers from './components/allUsers'
import editUser from './components/editUser'
import Home from './components/home'

class App extends Component {

  
  render() {
    return (
      <Router>
        <div className="App">
          <Route exact path='/' component={Home}/>
          <Route exact path='/new' component={Novousuario}/>
          {/* <Route exact path='/new' component={newUser}/> */}
          <Route exact path='/allUsers' component={allUsers}/>
          <Route path path='/editUser/:_id' component={editUser}/>
          
        </div>
      </Router>
    );
  }
}

export default App;