import React, { Component } from 'react'

class newUser extends Component{

    constructor(props){
        super(props)

        this.state = {
            users: [],
            loading: false
        }
        this.saveUser = this.saveUser.bind(this)
    }

    componentDidMount() {


      }

    saveUser(){
        const newUser = {
            name: this.refs.name.value,
            email: this.refs.email.value,
            address: this.refs.address.value,
            city: this.refs.city.value,
            state: this.refs.state.value,
        }
        console.log(newUser)
        fetch('/auth/register',{
            method: 'POST',
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(newUser)
            
        })
    }


    render(){
        return(
            <div className="newuser">
                <h1>Novo usuario</h1>
                <form>
                    Name: <input type="text" ref='name' className="form" /><br/>
                    E-mail: <input type="email" ref='email' className="form" /><br/>
                    Address: <input type="text" ref='address' className="form" /><br/>
                    City: <input type="text" ref='city' className="form" /><br/>
                    State: <input type="text" ref='state' className="form" /><br/>
                    <button onClick={this.saveUser}>Salvar</button>
                </form>

            </div>
        )
    }
}

export default newUser