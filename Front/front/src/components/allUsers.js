import React, {Component} from 'react';
import './allUsers.js';
import { Link } from 'react-router-dom'
import { throwStatement } from '@babel/types';
import { Redirect } from 'react-router-dom'

class allUsers extends Component {
    renderUsers(User){
        return(

        <div key={User._id} className="item  col-xs-4 col-lg-4">
          <div className="app">
            
            <div className="caixa">
              <h4 className="text">Name: {User.name}</h4>
                <h4 className="text">
                E-mail: {User.email}</h4>
                <h4 className="text">
                Address: {User.address}</h4>
                <h4 className="text">
                City: {User.city}</h4>
                <h4 className="text">
                State: {User.state}</h4>
                <h4 className="text">
                CreatedAt: {User.createdAt}</h4>
              <div className="row">
                <div className="card">
                </div>
                <div className="boton">
                  <Link className="butn" to={'/editUser/'+ User._id}>Editar</Link>
                  <a className="butn" onClick={() => this.deleteUser(User._id)} > Excluir</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        )

    }

  constructor(props){
    super(props)

    this.state = {
      loading: true,
      User: "vazio",
      redirect: false
  
  }

    this.renderUsers = this.renderUsers.bind(this)
    this.loadData = this.loadData.bind(this)

}





async deleteUser(_id){
  this.setState({loading : true})
  const delUser = {
    _id: _id
}
  const url = ('/auth/delete')
  const response = await fetch(url,{
    method: 'POST',
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(delUser)
  });
  const data = await response.json()


  // // const reload = await this.loadData()
  console.log(this.state)
  


}




async componentDidMount() {
  this.loadData()

  }


async loadData(){
  this.setState({loading : true})
  const url = ('/auth/getusers')
  var response = await fetch(url);
  const data = await response.json()
  const set = await this.setState({ User: data.usuario, loading: false})
  
}


  render() {
    return (
        <div>

         <div id="users" className="lista">
          
             { !this.state.loading && 
                this.state.User.map(this.renderUsers)
            }
         </div>


        </div>

    );
  }
}

export default allUsers;