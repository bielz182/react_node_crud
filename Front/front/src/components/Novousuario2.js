import React, { Component } from 'react'
import { tsImportEqualsDeclaration } from '@babel/types';
import './Novousuario.css'
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router-dom'

class Novousuario extends Component {
    constructor(props){
        super(props)

        this.state = {

            loading: true,
            redirect: false

        }
        this.postApi=this.postApi.bind(this)
    }

    componentDidMount() {
        return false
      }    


   async postApi(){
        
        const newUser = {
            name: this.refs.name.value,
            email: this.refs.email.value,
            address: this.refs.address.value,
            city: this.refs.city.value,
            state: this.refs.state.value,
        }
        console.log(newUser)
        if (this.refs.name.value != "" && this.refs.email.value != "" && this.refs.city.value != "" && this.refs.state.value != "" && this.refs.address.value != "" ){
            const response = await fetch('/auth/register',{
            method: 'POST',
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(newUser)

        })
        const body = await response.json()
        const validacao = await validacao(response)  
        
    }
        
        
}

async validacao(response){
    if (response.status === 200) {  
        alert("Cadastro Criado")
        this.setState({ redirect:'allUsers'})
       
        return response.json()

    }else if (response.status === 400){
        alert("Email ja cadastrado")
        return response

    }else if (response.status === 404){
        alert("Email nao e valido")
        return response
    }

  }

    botao(){
        console.log("add")
    }

    


    render(){
        return(
           
            <div className="newuser">
                 { this.state.redirect &&
                     <Redirect to={this.state.redirect}/>
                }
                <h1 className ="header">Novo usuario</h1>
                <form>
                    Name: <input type="text" ref='name' className="form" /><br/>
                    E-mail: <input type="email" ref='email' className="form" /><br/>
                    Address: <input type="text" ref='address' className="form" /><br/>
                    City: <input type="text" ref='city' className="form" /><br/>
                    State: <input type="text" ref='state' className="form" /><br/>
                    <br></br>
                    <br></br>
                    <a input type="button" className="butn" onClick={() => this.postApi()}> Save</a>
                    <Link className="butn" to={'/'}>Home</Link>
                </form>

            </div>
            
        )
    }
}

export default Novousuario