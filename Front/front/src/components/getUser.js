import React, {Component} from 'react';
import './getUser.js';

class getUser extends Component {
  constructor(props){
    super(props)

    this.state = {

    }
    this.callApi=this.callApi.bind(this)
}


  callApi = async () => {
    const response = await fetch('/oi2');
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    console.log(body.usuario)
    return body;
  };

  

  render() {
    return (
      <div className="User">
        <header className="App-header">
          <h1 className="search">Search User</h1>
        </header>
        <p>Email: <input type="email" ref='email' className="form" /><br/> </p>
        <button onClick={this.callApi}>Salver</button>
      </div>
    );
  }
}

export default getUser;