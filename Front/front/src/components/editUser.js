import React, { Component } from 'react'
import { tsImportEqualsDeclaration, throwStatement } from '@babel/types';
import './editUser.js'
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router-dom'

class editUser extends Component {
    constructor(props){
        super(props)
        this.state = {

            loading: false,
            User: null,

          }
        this.postApi=this.postApi.bind(this)
        this.componentDidMount=this.componentDidMount.bind(this)
         
     }

    
    async componentDidMount() {

        const response = await fetch('/auth/user',{
            method: 'GET',
            headers: { "Content-Type": "application/json",
            "_id" : (this.props.match.params._id)
             }
        });
        const body = await response.json();
        const edit = await this.setState({ User: body.usuario[0]})
        this.refs.name.value = this.state.User.name
        this.refs.email.value = this.state.User.email
        this.refs.address.value = this.state.User.address
        this.refs.city.value = this.state.User.city
        this.refs.state.value = this.state.User.state
        if (response.status !== 200) throw Error(body.message);
        console.log(this.state)
        return body;

    }



    // Funcao que faz o post no back-end
    postApi(){
        const newUser = {
            name: this.refs.name.value,
            email: this.refs.email.value,
            address: this.refs.address.value,
            city: this.refs.city.value,
            state: this.refs.state.value,
        }
        console.log(newUser)
        fetch('/auth/edit',{
            method: 'POST',
            headers: { "Content-Type": "application/json",
            "_id" : (this.props.match.params._id) },
            body: JSON.stringify(newUser)
        })
        
        .then(function(response){
            alert("Usuario editado com sucesso")
            return response.json()
        })
        .then(function(data){
            
            console.log('post request response data', data)

        })
    }

    


    render(){
        return(
            <div className="newuser">
                <h1>Edit User</h1>
                <p></p>
                <form>
                    <table align ="center">
                    <tr>
                    <th>Name:</th> <td><input type="text"  ref='name' className="form" placeholder="Name"/></td><br/>
                    </tr>
                    
                    <tr>
                    <th>E-mail:</th> <input type="email" ref='email' className="form" placeholder="Name"/><br/>
                    </tr>
                    <tr>
                    <th>Address:</th>  <input type="text" ref='address' className="form"placeholder="Name" /><br/>
                    </tr>
                    <tr>
                    <th>City:</th>  <input type="text" ref='city' className="form" placeholder="Name"/><br/>
                    </tr>
                    <th>State:</th>  <input type="text" ref='state' className="form" placeholder="Name"/><br/>
                    <button className="butn" onClick={this.postApi}>Editar</button>
                    <Link className="butn" to={'/'}>Home</Link> 
                    
                    </table>
                    
                </form>

            </div>
        )
    }
}

export default editUser;