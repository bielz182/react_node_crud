import React, { Component } from 'react'
import { tsImportEqualsDeclaration } from '@babel/types';
import './Novousuario.css'
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router-dom'

class Novousuario extends Component {
    constructor(props){
        super(props)

        this.state = {

            loading: true,
            redirect: false

        }
        this.postApi=this.postApi.bind(this)
    }

    componentDidMount() {
        return false
      }    

    postApi(){
        
        const newUser = {
            name: this.refs.name.value,
            email: this.refs.email.value,
            address: this.refs.address.value,
            city: this.refs.city.value,
            state: this.refs.state.value,
        }
        console.log(newUser)
        if (this.refs.name.value != "" && this.refs.email.value != "" && this.refs.city.value != "" && this.refs.state.value != "" && this.refs.address.value != "" ){
        fetch('/auth/register',{
            method: 'POST',
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(newUser)
            
        })        
        .then(function(response){            
            console.log(response.status)
            if (response.status === 200) {  
                alert("Cadastro Criado")
               
                return response.json()

            }else if (response.status === 400){
                alert("Email ja cadastrado")
                return response

            }else if (response.status === 404){
                alert("Email nao e valido")
                return response
            }

            this.setState({
                redirect: '/allUsers'
            })
        })
        .then(function(data){
            console.log('post request response data', data)
            console.log(data.status)
            
            

        })
        return false
    }else{
        alert("Digite todos os campos.")
        return false
    }
        
}

    botao(){
        console.log("add")
    }

    


    render(){
        return(
           
            <div className="newuser">
                 { this.state.redirect &&
                     <Redirect to={this.state.redirect}/>
                }
                <h1 className ="header">Novo usuario</h1>
                <form>
                <table align ="center">
                    <tr>
                    <th>Name:</th> <td><input type="text"  ref='name' className="form" placeholder="Name"/></td><br/>
                    </tr>
                    
                    <tr>
                    <th>E-mail:</th> <input type="email" ref='email' className="form" placeholder="E-mail"/><br/>
                    </tr>
                    <tr>
                    <th>Address:</th>  <input type="text" ref='address' className="form"placeholder="Address" /><br/>
                    </tr>
                    <tr>
                    <th>City:</th>  <input type="text" ref='city' className="form" placeholder="City"/><br/>
                    </tr>
                    <th>State:</th>  <input type="text" ref='state' className="form" placeholder="State"/><br/>
                    
                    <a input type="button" className="butn" onClick={() => this.postApi()}> Save</a>
                    <Link className="butn" to={'/'}>Home</Link>

                    </table>
                </form>

            </div>
            
        )
    }
}

export default Novousuario