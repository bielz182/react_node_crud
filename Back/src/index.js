const express = require('express')
const bodyParser = require('body-parser')

const app = express()

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}))


app.get('/oi', (req, res)=>{
    res.send({express: 'AGROA FOIIII'})
})

app.get('/oi2', function(req, res){
    res.json([
        {id: 1, username: "alguem"},
        {id:2, username: "outro alguem"}
        
        
    ])
})

require('./controllers/authController')(app);



app.listen(4000);
