const express = require('express');

const validator = require('validator')

const User = require('../models/user');

const router = express.Router();

router.post('/register', async (req, res) =>{
    console.log(req.body)
    if(validator.isEmail(req.body.email) ){
        try{
            const user = await User.create(req.body);

            return res.status(200).send({ user });
        }catch (err){
            return res.status(400).send({error: "Email ja cadastrado"})
        }
    }else{
        console.log("Falha no registro")
        return res.status(404).send({error: "Falha no registro, email nao e valido"})
    }
})


router.get('/user', async (req, res) =>{
    // console.log(req.body, "BODY")
    console.log(req.headers._id, "HEADERS")
    // console.log(req.query, "QUERY")
    User.find({"_id":req.headers._id}, function (err, usuario) {

        if(usuario < 1){
            res.send({
                'status': "error",
                'message': "User not found."
            });

        }else{ 
            console.log(usuario) 
        res.send({
            usuario
         })
        }
    })

})

router.get('/getusers', async (req, res) =>{
    
    User.find({}, function (err, usuario) {

        if(usuario < 1){
            res.send({
                'status': "error",
                'message': "User not found."
            });

        }else{  
        console.log(usuario)
        res.send({
            usuario
        
         })
        }
    })

})


router.post('/edit', async (req, res) =>{
    console.log(req.headers._id)
    console.log(req.body.name)


    User.updateOne({_id: req.headers._id}, {$set: {name:req.body.name, 
        email:req.body.email, 
        address:req.body.address, 
        city:req.body.city }}).exec().then(()=>{
        res.send("Registro alterado")
    })
    
    


})

router.post('/delete', async (req, res) =>{
    
    User.remove({"_id":req.body._id}, function (err, usuario) {

        if(usuario < 1){
            res.send({
                'status': "error",
                'message': "User not found."
            });

        }else{  
            console.log("Registro deletado", usuario)
        }
    })

})




module.exports = app => app.use('/auth', router)

